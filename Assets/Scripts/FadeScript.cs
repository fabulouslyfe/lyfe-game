﻿using UnityEngine;
using System.Collections;

public class FadeScript : ElementX {

	private SpriteRenderer spriteRenderer;
	public float totalTime = 3.0f;
	public float fadeTime = 0.5f;
	public bool loadMenuOnZero = true;
	float timer;

	void Start() {
		spriteRenderer = GetComponent<SpriteRenderer>();
		timer = totalTime;
	}

	void Update() {
		timer -= Time.deltaTime;
		if(timer < 0.0f) {
			if(loadMenuOnZero) {
				Notify("start.mainmenu");
			} else {
				Destroy(gameObject);
				return;
			}
		} else if(timer < fadeTime) {
			Color tmp = spriteRenderer.color;
			tmp.a = timer / fadeTime;
			spriteRenderer.color = tmp;
		}
	}
}
