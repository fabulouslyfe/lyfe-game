﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalConfig : ElementX {

	public readonly List<float> ROUND_TIME = new List<float>() { 46.0f, 46.0f, 46.0f, 41.0f, 31.0f, 31.0f };
	public readonly List<float> WIN_THRESHOLD_AREA = new List<float>() { 14.0f, 17.0f, 18.5f, 21.0f, 22.0f, 22.0f };

	public bool fullScreen = true;
	public int screenWidth = 720;
	public int screenHeight = 1280;
	public bool sfx = true;
	public bool bgm = true;

	void Start() {
		//Screen.SetResolution(screenWidth, screenHeight, fullScreen);
	}

}
