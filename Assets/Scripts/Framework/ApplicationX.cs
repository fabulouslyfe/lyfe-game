﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class ApplicationX : ElementX {
	public bool debugOnScreen = false;

	public ModelX model { get { return m_model = Assert<ModelX>(m_model); } }

	private ModelX m_model;

	public ViewX view { get { return m_view = Assert<ViewX>(m_view); } }

	private ViewX m_view;

	public ControllerX controller { get { return m_controller = Assert<ControllerX>(m_controller); } }

	private ControllerX m_controller;

	public GlobalConfig globalConfig { get { return m_globalConfig = (Assert<GlobalConfig>(m_globalConfig)).GetComponent<GlobalConfig>(); } }

	private GlobalConfig m_globalConfig;

	private BGMController bgmController;

	public int verbose;

	/// Flag that indicates the first scene was loaded.
	static bool m_first_scene;

	/// Arguments to be passed between scenes.
	static List<string> __args;

	/// Arguments passed between scenes.
	public List<string> args { get { return __args == null ? (new List<string>()) : __args; } }

	/// Wrapper for the current scene's id.
	public int levelId {
		get {
			return SceneManager.GetActiveScene().buildIndex;
		}
	}

	/// Wrapper for the current scene's name.
	public string levelName {
		get {
			return SceneManager.GetActiveScene().name;
		}
	}

	/// Async data structures.
	private List<UnityEngine.AsyncOperation> m_async_loads { get { return __async_loads == null ? (__async_loads = new List<UnityEngine.AsyncOperation>()) : __async_loads; } }

	private List<UnityEngine.AsyncOperation> __async_loads;

	private List<string> m_async_args { get { return __async_args == null ? (__async_args = new List<string>()) : __async_args; } }

	private List<string> __async_args;

	/// Initialization.
	virtual protected void Start() {
		__async_loads = new List<UnityEngine.AsyncOperation>();
		__async_args = new List<string>();
		if(m_first_scene) {
			m_first_scene = false;
			OnLevelLoaded(levelId);
		}
		Notify("scene.start", new object[] { levelName, levelId });
	}

	/// Capture the level loaded event and notify controllers for 'starting' purposes.
	private void OnLevelLoaded(int p_level) {
		Notify("scene.load", new object[] { levelName, levelId });
	}

	/// Notifies all application's controllers informing who's the 'target' and passing some 'data'.
	public void Notify(string p_event, Object p_target, params object[] p_data) {
		ControllerX root = transform.GetComponentInChildren<ControllerX>();
		ControllerX[] list = root.GetComponentsInChildren<ControllerX>();
		Log(p_event + " [" + p_target + "]", 6);
		for(int i = 0; i < list.Length; i++)
			list[i].OnNotification(p_event, p_target, p_data);
		GameObject bgmControl = (GameObject.FindWithTag("BGMControl") as GameObject);
		if(bgmControl != null) {
			bgmController = bgmControl.GetComponent<BGMController>();
			bgmController.OnNotification(p_event, p_target, p_data);
		}
	}

	/// Notifies all application's controllers informing who's the 'target' after 'delay' in seconds and passing some 'data'.
	public void Notify(float p_delay, string p_event, Object p_target, params object[] p_data) {            
		StartCoroutine(TimedNotify(p_delay, p_event, p_target, p_data));
	}

	/// Internal Notify to help timed notifications.
	private IEnumerator TimedNotify(float p_delay, string p_event, Object p_target, params object[] p_data) {
		yield return new WaitForSeconds(p_delay);
		Notify(p_event, p_target, p_data);
	}

	/// Adds a new scene by name. An async flag can control the load type.
	public void SceneAdd(string p_name, bool p_async, params string[] p_args) {
		if(p_async) {
			StartCoroutine(SceneLoadAsync(p_name, true, p_args));
		} else {
			__args = new List<string>(p_args);
			SceneManager.LoadScene(p_name, LoadSceneMode.Additive);
		}
	}

	/// Adds a new scene.
	public void SceneAdd(string p_name, params string[] p_args) {
		SceneAdd(p_name, false, p_args);
	}

	/// Loads a new scene by name. A flag indicating if the load must be async can be informed.
	public void SceneLoad(string p_name, bool p_async, params string[] p_args) {
		if(p_async) {
			StartCoroutine(SceneLoadAsync(p_name, false, p_args));
		} else {
			__args = new List<string>(p_args);
			SceneManager.LoadScene(p_name, LoadSceneMode.Single);
		}
	}

	/// Loads a new scene by name.
	public void SceneLoad(string p_name, params string[] p_args) {
		SceneLoad(p_name, false, p_args);
	}

	/// Internal method for async load level.
	private IEnumerator SceneLoadAsync(string p_name, bool p_additive, params string[] p_args) {
		//float p = 0f;
		UnityEngine.AsyncOperation async = null;
		string ev = "";

		if(p_additive) {
			ev = "scene.add.progress";
			async = SceneManager.LoadSceneAsync(p_name, LoadSceneMode.Additive);
		} else {
			ev = "scene.load.progress";
			async = SceneManager.LoadSceneAsync(p_name, LoadSceneMode.Single);
		}

		m_async_loads.Add(async);
		m_async_args.Add(p_name + "~" + ev);

		yield return async;
		__args = new List<string>(p_args);
	}

	/// Update some internal states.
	void Update() {
		for(int i = 0; i < m_async_loads.Count; i++) {

			UnityEngine.AsyncOperation async = m_async_loads[i];

			if(async != null) {
				string args = m_async_args[i];
				string s_name = args.Split('~')[0];
				string s_ev = args.Split('~')[1];
				if(s_ev != "")
					Notify(s_ev, new object[] { s_name, async.progress });
				if(async.progress >= 1.0)
					m_async_loads[i] = null;                    
			} else {
				m_async_loads.RemoveAt(i--);
				m_async_args.RemoveAt(i--);
			}
		}
	}
}