﻿using UnityEngine;
using System.Collections;

// Base class for all elements in this application.
public class ElementX : MonoBehaviour {
	string debug = "";

	public ApplicationX app {
		get {
			return GameObject.FindObjectOfType<ApplicationX>();
		}
	}

	/// Finds a instance of 'T' if the named 'var' is null. Returns the storage['var'] otherwise.
	/// If 'global' is 'true' searches in all scope, otherwise, searches in childrens.
	public T Assert<T>(T p_var, bool p_global = false) where T : Object {
		return p_var == null ? (p_global ? GameObject.FindObjectOfType<T>() : transform.GetComponentInChildren<T>()) : p_var;            
	}

	/// Sends a notification to all controllers passing this instance as 'target'.
	public void Notify(string p_event) {
		app.Notify(p_event, this, new object[] { });
	}

	/// Sends a notification to all controllers passing this instance as 'target' and some 'data'.
	public void Notify(string p_event, params object[] p_data) {
		app.Notify(p_event, this, p_data);
	}

	/// Sends a notification to all controllers, after 'delay', passing this instance as 'target' and some 'data'.
	public void Notify(float p_delay, string p_event, params object[] p_data) {
		app.Notify(p_delay, p_event, this, p_data);
	}

	/// Logs a message using this element information.
	public void Log(object p_msg, int p_verbose = 0) {
		//Only outputs logs equal or bigger than the application 'verbose' level.
		if(p_verbose <= app.verbose) {
			Debug.Log(GetType().Name + "> " + p_msg);
			if(app.debugOnScreen) {
				debug = p_msg + "\n" + debug;
			}
		}
	}

	void OnGUI() {
			GUI.contentColor = Color.white;
			GUI.Label(new Rect(0, 0, 200, 400), debug);
		}
}