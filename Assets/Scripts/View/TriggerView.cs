﻿using UnityEngine;
using System.Collections;

public class TriggerView : ViewX {

	public bool triggered = false;

	void OnTriggerEnter2D(Collider2D coll) {
		if(!triggered) {
			Notify("trigger.enter", new Object[] { coll });
			triggered = true;
		}
	}

	void OnTriggerExit2D(Collider2D coll) {
		if(triggered) {
			Notify("trigger.exit", new Object[] { coll });
			triggered = false;
		}
	}
}
