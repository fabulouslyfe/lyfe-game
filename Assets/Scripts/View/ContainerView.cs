﻿using UnityEngine;
using System.Collections;

public class ContainerView : ViewX {
	
	public int totalItemInserted = 4;
	public int currentItemInserted;

	void OnTriggerEnter2D(Collider2D coll) {
		Notify("item.collide.tile", new Object[] { coll });   
	}
}
