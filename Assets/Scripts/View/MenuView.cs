﻿using UnityEngine;
using System.Collections;

public class MenuView : ViewX {

	public void StartLevel(int level) {
		Notify("start.level" + level);
	}

	public void StartLevelMenu() {
		Notify("start.levelmenu");
	}

	public void BackToMainMenu() {
		Notify("level.resumed");
		Notify("start.mainmenu");
	}

	public void OpenLevelMenu(int level) {
		Notify("open.levelmenu" + level);
	}

	public void StartSettings() {
		Notify("start.settings");
	}

	public void TempStartLevelAdrian() {
		Notify("start.adrian");
	}

	public void TempStartLevelChris() {
		Notify("start.chris");
	}

	public void TempStartLevelLily() {
		Notify("start.lily");
	}

	public void TempStartLevelMgs() {
		Notify("start.mgs");
	}

	public void TempStartLevelShylla() {
		Notify("start.shylla");
	}

	public void ExitGame() {
		Debug.Log("exit");
		Application.Quit();
	}

	public void NextLevel() {
		Notify("next.level");
	}

	public void PlayAgain() {
		Notify("play.again");
	}

	public void NextDialogue() {
		Notify("next.dialogue");
	}

	public void OpenCutscene(int level) {
		Notify("open.cutscene" + level);
	}

	public void PauseGame() {
		Notify("level.paused");
	}

	public void ResumedGame() {
		Notify("level.resumed");
	}

	public void RestartGame() {
		Notify("level.resumed");
		Notify("play.again");
	}
}
    