﻿using UnityEngine;
using System.Collections;

public class ItemView : ViewX {
	public int id;
	private Vector3 screenPoint;
	private Vector3 offset;
	public bool canDrag = true;
	public bool mouseDrag = false;
	private Vector3 initPosition;
	public int totalCollide;
	public int currentCollide;
	public bool isInserted = false;
	public int spawnSlot;
	bool isOverlapping = false;
	bool hitTrash = false;
	private TriggerView[] triggerChildren;
	private SpriteRenderer spriteRenderer;

	void Start() {
		triggerChildren = GetComponentsInChildren<TriggerView>();
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	void OnMouseDown() {
		initPosition = gameObject.transform.position;
		if(!isInserted) {
			Notify("will.select.item");
		}
		if(canDrag) {
			Notify("sfx.click");
			mouseDrag = true;
			offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
			spriteRenderer.sortingOrder = 5;
		}
	}

	void OnMouseDrag() {
		if(canDrag || (!canDrag && mouseDrag)) {
			Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
			Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
			transform.position = curPosition;
			if(totalCollide == countTriggered() && !isOverlapping) {
				Notify("hightlight.item.canInsert");
               
			} else {
				Notify("hightlight.item.cannotInsert");
			}
		}
	}

	void OnMouseUp() {
		Notify("sfx.click");
		if(hitTrash) {
			Notify("item.thrown", new object[] { spawnSlot });
			Destroy(gameObject);
			return;
		}
		if(totalCollide == countTriggered() && !isOverlapping) {
			canDrag = false;
			spriteRenderer.sortingOrder = 1;
			Notify("item.inserted", new object[] { spawnSlot, triggerChildren.Length });
			transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
			isInserted = true;
			// TODO: FIX THIS MAGIC CONSTANT
			GetComponent<Rigidbody2D>().drag = 9999999;
			GetComponent<Rigidbody2D>().mass = 9999999;
		} else {
			Notify("select.item");
			isInserted = false; 
		}
		mouseDrag = false;
		if(!isInserted) {
			transform.position = initPosition;
		}
	}

	int countTriggered() {
		int numberOfTriggered = 0;
		foreach(TriggerView trigger in triggerChildren) {
			if(trigger.triggered) {
				numberOfTriggered++;
			}
		}
		return numberOfTriggered;
	}

	void OnCollisionStay2D(Collision2D coll) {
		if(coll.gameObject.CompareTag("Item")) {
			isOverlapping = true;
		}
	}

	void OnCollisionExit2D(Collision2D coll) {
		if(coll.gameObject.CompareTag("Item")) {
			isOverlapping = false;
		}
	}

	void OnTriggerStay2D(Collider2D coll) {
		if(coll.gameObject.CompareTag("Trash")) {
			hitTrash = true;
		}
	}

	void OnTriggerExit2D(Collider2D coll) {
		if(coll.gameObject.CompareTag("Trash")) {
			hitTrash = false;
		}
	}
}
