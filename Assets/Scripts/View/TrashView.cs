﻿using UnityEngine;
using System.Collections;

public class TrashView : ViewX {

	public bool ready;

	void Start() {
		ready = false;
	}

	void OnMouseOver() {
		if(ready) {
			transform.localScale = new Vector3(0.25f, 0.25f, 1f);
		}
	}

	void OnMouseExit() {
		transform.localScale = new Vector3(0.175f, 0.175f, 1f);
	}
}
