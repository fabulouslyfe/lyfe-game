﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : ControllerX {

	float timeLeft;
	bool isTimerStarted;
	bool isRoundStarted;
	List<GameObject> items;
	public int numberOfItems;
	GameObject container;
	int score = 0;
	public GameObject scoreView;
	public GameObject timeView;
	public GameObject winModal;
	public GameObject loseModal;
	public GameObject containerBarView;
	List<GameObject> itemsToSpawn;
	float containerArea;
	float totalItemArea;
	float totalItemInsertedArea;
	private int currentLevel;
	List<GameObject> spawnedItems;
	GameObject[] pauseObjects;

	void Start() {
		timeLeft = 3.0f;
		isTimerStarted = true;
		items = new List<GameObject>();
		itemsToSpawn = new List<GameObject>();
		spawnedItems = new List<GameObject>();
		scoreView.GetComponent<UnityEngine.UI.Text>().text = "$" + score;
		winModal.SetActive(false);
		loseModal.SetActive(false);
		pauseObjects = GameObject.FindGameObjectsWithTag("ShowOnPaused");
		hidePaused();
		Notify("play.game.bgm");
	}

	void Update() {
		if(isTimerStarted) {
			timeLeft -= Time.deltaTime;
			if(isRoundStarted) {
				// Round timer
				timeView.GetComponent<UnityEngine.UI.Text>().text = convertTime((int)timeLeft);
			} else {
				// Countdown timer
				if(timeLeft > 0) {
					int timeDisplay = (int)timeLeft + 1;
					timeView.GetComponent<UnityEngine.UI.Text>().text = "" + timeDisplay;
				}
			}
			if(timeLeft < 0) {
				if(isRoundStarted) {
					// Game Over
					lose();
				} else {
					timeLeft = app.globalConfig.ROUND_TIME[currentLevel];
					isRoundStarted = true;
					SpawnContainer();
					LoadAllItems();
					SpawnItemsToAllSlot();
					Notify("round.start");
				}
			}

		}
	}

	override public void OnNotification(string p_event, Object p_target, params object[] p_data) {
		switch(p_event) {
			case "scene.start":
				if(app.args.Count > 0) {
					string scene = app.args[0];
					currentLevel = int.Parse(scene.Substring(5, 1));
					isTimerStarted = true;
				}
				break;
			case "item.thrown":
				ItemView itemView = ((ItemView)p_target);
				SpawnItem((int)p_data[0], itemView.id);
				break;
			case "level.complete":
				win();
				break;
			case "item.inserted":
				// Spawn New Item
				// TODO Check this part, item ke-4 = container
				totalItemInsertedArea += ((ItemView)p_target).gameObject.GetComponent<SpriteRenderer>().bounds.size.magnitude;
				containerBarView.GetComponent<ContainerBarView>().pict.fillAmount = totalItemInsertedArea / containerArea;
				Log("itemInserted/container: " + totalItemInsertedArea + "/" + containerArea);
				float winThreshold = 0.95f;
				if(totalItemInsertedArea < containerArea * winThreshold) {
					SpawnItem((int)p_data[0]);
					// Update Score
					int itemScore = (int)p_data[1] * 10;
					int timeScore = (int)timeLeft * 2;
					score += itemScore + timeScore;
					scoreView.GetComponent<UnityEngine.UI.Text>().text = "$" + score;
				} else {
					win();
				}
				break;
			case "level.paused":
				Time.timeScale = 0;
				showPaused();
				break;
			case "level.resumed":
				Time.timeScale = 1;
				hidePaused();
				break;
		}
	}

	void win() {
		winModal.transform.GetChild(1).GetComponent<UnityEngine.UI.Text>().text = "Your Score : " + score;
		winModal.SetActive(true);
		DisableAllItems();
	}

	void lose() {
		loseModal.transform.GetChild(1).GetComponent<UnityEngine.UI.Text>().text = "Your Score : " + score;
		loseModal.SetActive(true);
		DisableAllItems();
	}

	void DisableAllItems() {
		foreach(GameObject item in spawnedItems) {
			item.SetActive(false);
		}
	}

	string convertTime(int a) {
		int aa = a / 60;
		int ab = a % 60;
		string minutes = aa.ToString();
		string seconds = ab.ToString();
		string ha = "" + minutes + ":" + seconds;
		return ha;
	}

	void LoadAllItems() {
		containerArea = app.globalConfig.WIN_THRESHOLD_AREA[currentLevel];
		totalItemArea = 0;
		for(int i = 0; i < numberOfItems; i++) {
			// TODO: 4 >> currentLevel
			GameObject itemToAdd;
			if(currentLevel < 5) {
				itemToAdd = Resources.Load("level" + currentLevel + "item" + i) as GameObject;
			} else {
				itemToAdd = Resources.Load("level" + 4 + "item" + i) as GameObject;
			}
			items.Add(itemToAdd);
		}
		foreach(GameObject item in items) {
			GameObject itemToSpawn = item;
			totalItemArea += itemToSpawn.GetComponent<SpriteRenderer>().bounds.size.magnitude;
			if(totalItemArea < containerArea) {
				itemsToSpawn.Add(itemToSpawn);
			} else {
				break;
			}
		}
	}

	int RandomInt(int start, int end, int excludeIndex) {
		int randomInt = Random.Range(0, itemsToSpawn.Count);
		if(excludeIndex == randomInt) {
			return RandomInt(start, end, excludeIndex);
		} else {
			return randomInt;
		}
	}

	void SpawnItem(int slot, int excludeIndex = -1) {
		int randomPick = RandomInt(0, itemsToSpawn.Count, excludeIndex);
		GameObject item = Instantiate(itemsToSpawn[randomPick]) as GameObject;
		ItemView itemView = item.GetComponent<ItemView>();
		item.transform.parent = app.view.transform;
		itemView.id = randomPick;
		if(slot == 0) {
			item.transform.position = new Vector3(-1.7f, -2.7f, 5.0f);
			itemView.spawnSlot = 0;
		} else if(slot == 1) {
			item.transform.position = new Vector3(0.0f, -2.7f, 5.0f);
			itemView.spawnSlot = 1;
		} else {
			item.transform.position = new Vector3(1.7f, -2.7f, 5.0f);
			itemView.spawnSlot = 2;
		}
		spawnedItems.Add(item);
	}

	void SpawnItemsToAllSlot() {
		for(int slot = 0; slot < 3; slot++) {
			SpawnItem(slot);
		}
	}

	void SpawnContainer() {
		// TODO: 1 >> currentLevel
		container = Instantiate(Resources.Load("level" + 1 + "container")) as GameObject;
		container.transform.parent = app.view.transform;
		container.transform.position = new Vector3(0.0f, 1.5f);
		Notify("container.initialized", new Object[] { container });
	}

	void hidePaused() {
		foreach(GameObject g in pauseObjects) {
			g.SetActive(false);
		}
	}

	void showPaused() {
		foreach(GameObject g in pauseObjects) {
			g.SetActive(true);
		}
	}
}
