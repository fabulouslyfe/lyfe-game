﻿using UnityEngine;
using System.Collections;

public class TrashController : ControllerX {

	public GameObject trashcan;
	private TrashView trashView;

	void Start() {
		trashView = trashcan.GetComponent<TrashView>();
	}

	public override void OnNotification(string p_event, Object p_target, params object[] p_data) {
		switch(p_event) {
			case "item.selected":
				trashView.ready = true;
				break;
			case "item.unselected":
				trashView.ready = false;
				break;
			case "item.thrown":
				trashView.ready = false;
				break;
		}
	}
}
