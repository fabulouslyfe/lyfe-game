﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TutorialController : ControllerX {

	float timeLeft;
	bool isTimerStarted;
	bool isRoundStarted;
	List<GameObject> items;
	public int numberOfItems;
	GameObject container;
	int score = 0;
	public GameObject scoreView;
	public GameObject timeView;
	public GameObject winModal;
	public GameObject loseModal;
	public GameObject containerBarView;
	List<GameObject> itemsToSpawn;
	float containerArea;
	float totalItemArea;
	float totalItemInsertedArea;
	private int currentLevel;
	private bool firstInsert = false;
	public GameObject tutSelect;
	public GameObject tutRotate;
	public GameObject tutRotated;
	public GameObject tutDrag;
	public GameObject tutCantMove;
	public GameObject tutPlay;
	private bool isAlreadyRotated = false;
	GameObject[] pauseObjects;

	void Start() {
		timeLeft = 3.0f;
		isTimerStarted = true;
		items = new List<GameObject>();
		itemsToSpawn = new List<GameObject>();
		scoreView.GetComponent<UnityEngine.UI.Text>().text = "$" + score;
		winModal.SetActive(false);
		loseModal.SetActive(false);
		pauseObjects = GameObject.FindGameObjectsWithTag("ShowOnPaused");
		hidePaused();
		Notify("play.game.bgm");
	}

	void Update() {
		if(isTimerStarted) {
			timeLeft -= Time.deltaTime;
			if(isRoundStarted) {
				// Round timer
				timeView.GetComponent<UnityEngine.UI.Text>().text = convertTime((int)timeLeft);
				if(!firstInsert) {
					isTimerStarted = false;
				}
			} else {
				// Countdown timer
				if(timeLeft > 0) {
					int timeDisplay = (int)timeLeft + 1;
					timeView.GetComponent<UnityEngine.UI.Text>().text = "" + timeDisplay;
				}
			}
			if(timeLeft < 0) {
				if(isRoundStarted) {
					// Game Over
					lose();
				} else {
					timeLeft = app.globalConfig.ROUND_TIME[currentLevel];
					isRoundStarted = true;
					tutSelect.SetActive(true);
					SpawnContainer();
					LoadAllItems();
					SpawnItemsToAllSlot();
					Notify("round.start");
				}
			}

		}
	}

	override public void OnNotification(string p_event, Object p_target, params object[] p_data) {
		switch(p_event) {
			case "scene.start":
				if(app.args.Count > 0) {
					string scene = app.args[0];
					currentLevel = int.Parse(scene.Substring(5, 1));
					isTimerStarted = true;
				}
				break;
			case "item.thrown":
				ItemView itemView = ((ItemView)p_target);
				SpawnItem((int)p_data[0], itemView.id);
				break;
			case "level.complete":
				win();
				break;
			case "item.inserted":
				// Spawn New Item
				// TODO Check this part, item ke-4 = container
				totalItemInsertedArea += ((ItemView)p_target).gameObject.GetComponent<SpriteRenderer>().bounds.size.magnitude;
				containerBarView.GetComponent<ContainerBarView>().pict.fillAmount = totalItemInsertedArea / containerArea;
				Log("itemInserted/container: " + totalItemInsertedArea + "/" + containerArea);
				float winThreshold = 0.95f;
				if(totalItemInsertedArea < containerArea * winThreshold) {
					SpawnItem((int)p_data[0]);
					// Update Score
					int itemScore = (int)p_data[1] * 10;
					int timeScore = (int)timeLeft * 2;
					score += itemScore + timeScore;
					scoreView.GetComponent<UnityEngine.UI.Text>().text = "$" + score;
					if(!firstInsert) {
						firstInsert = true;
						tutDrag.SetActive(false);
						tutRotated.SetActive(false);
						tutCantMove.SetActive(true);
						Invoke("showPlayTutorial", 3);
					}
				} else {
					win();
				}
				break;
			case "item.selected":
				if(!firstInsert) {
					if(!isAlreadyRotated) {
						tutSelect.SetActive(false);
						tutRotate.SetActive(true);
					}
				} else {
					tutSelect.SetActive(false);
					tutDrag.SetActive(false);
					tutRotate.SetActive(false);
					tutRotated.SetActive(false);
				}
				break;
			case "rotate.item":
				if(!isAlreadyRotated) {
					tutRotate.SetActive(false);
					tutRotated.SetActive(true);
					tutDrag.SetActive(true);
					isAlreadyRotated = true;
				}
				//Invoke("showDragTutorial", 3);
				break;
			case "level.paused":
				Time.timeScale = 0;
				showPaused();
				break;
			case "level.resumed":
				Time.timeScale = 1;
				hidePaused();
				break;
		}
	}

	void finishTutorial() {
		tutSelect.SetActive(false);
		tutDrag.SetActive(false);
		tutCantMove.SetActive(false);
		tutPlay.SetActive(false);
		tutRotate.SetActive(false);
		tutRotated.SetActive(false);
	}

	void showPlayTutorial() {
		tutCantMove.SetActive(false);
		tutPlay.SetActive(true);
		isTimerStarted = true;
		Invoke("finishTutorial", 3);
	}

	void showDragTutorial() {
		tutRotated.SetActive(false);
		tutDrag.SetActive(true);
	}

	void win() {
		winModal.transform.GetChild(1).GetComponent<UnityEngine.UI.Text>().text = "Your Score : " + score;
		winModal.SetActive(true);
	}

	void lose() {
		loseModal.transform.GetChild(1).GetComponent<UnityEngine.UI.Text>().text = "Your Score : " + score;
		loseModal.SetActive(true);
	}

	string convertTime(int a) {
		int aa = a / 60;
		int ab = a % 60;
		string minutes = aa.ToString();
		string seconds = ab.ToString();
		string ha = "" + minutes + ":" + seconds;
		return ha;
	}

	void LoadAllItems() {
		containerArea = app.globalConfig.WIN_THRESHOLD_AREA[currentLevel];
		totalItemArea = 0;
		for(int i = 0; i < numberOfItems; i++) {
			GameObject itemToAdd = Resources.Load("level" + 0 + "item" + i) as GameObject;
			items.Add(itemToAdd);
		}
		foreach(GameObject item in items) {
			GameObject itemToSpawn = item;
			totalItemArea += itemToSpawn.GetComponent<SpriteRenderer>().bounds.size.magnitude;
			if(totalItemArea < containerArea) {
				itemsToSpawn.Add(itemToSpawn);
			} else {
				break;
			}
		}
	}

	int RandomInt(int start, int end, int excludeIndex) {
		int randomInt = Random.Range(0, itemsToSpawn.Count);
		if(excludeIndex == randomInt) {
			return RandomInt(start, end, excludeIndex);
		} else {
			return randomInt;
		}
	}

	void SpawnItem(int slot, int excludeIndex = -1) {
		int randomPick = RandomInt(0, itemsToSpawn.Count, excludeIndex);
		GameObject item = Instantiate(itemsToSpawn[randomPick]) as GameObject;
		ItemView itemView = item.GetComponent<ItemView>();
		item.transform.parent = app.view.transform;
		itemView.id = randomPick;
		if(slot == 0) {
			item.transform.position = new Vector3(-1.7f, -2.7f, 5.0f);
			itemView.spawnSlot = 0;
		} else if(slot == 1) {
			item.transform.position = new Vector3(0.0f, -2.7f, 5.0f);
			itemView.spawnSlot = 1;
		} else {
			item.transform.position = new Vector3(1.7f, -2.7f, 5.0f);
			itemView.spawnSlot = 2;
		}
	}

	void SpawnItemsToAllSlot() {
		int pick;
		GameObject item;
		ItemView itemView;
		pick = 1;
		item = Instantiate(itemsToSpawn[pick]) as GameObject;
		itemView = item.GetComponent<ItemView>();
		item.transform.parent = app.view.transform;
		itemView.id = pick;
		item.transform.position = new Vector3(-1.7f, -2.7f, 5.0f);
		item.transform.Rotate(0.0f, 0.0f, -45.0f);
		itemView.spawnSlot = 0;

		pick = 4;
		item = Instantiate(itemsToSpawn[pick]) as GameObject;
		itemView = item.GetComponent<ItemView>();
		item.transform.parent = app.view.transform;
		itemView.id = pick;
		item.transform.position = new Vector3(0.0f, -2.7f, 5.0f);
		itemView.spawnSlot = 1;

		pick = 0;
		item = Instantiate(itemsToSpawn[pick]) as GameObject;
		itemView = item.GetComponent<ItemView>();
		item.transform.parent = app.view.transform;
		itemView.id = pick;
		item.transform.position = new Vector3(1.7f, -2.7f, 5.0f);
		itemView.spawnSlot = 2;
	}

	void SpawnContainer() {
		container = Instantiate(Resources.Load("level0container")) as GameObject;
		container.transform.parent = app.view.transform;
		container.transform.position = new Vector3(0.0f, 1.5f);
		Notify("container.initialized", new Object[] { container });
	}

	void hidePaused() {
		foreach(GameObject g in pauseObjects) {
			g.SetActive(false);
		}
	}

	void showPaused() {
		foreach(GameObject g in pauseObjects) {
			g.SetActive(true);
		}
	}
}
