﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CutSceneController : ControllerX {
	public int forLevel;
	List<string> cutsceneStrings;
	public GameObject nextText;
	int currentIndex = 0;

	void Start() {
		cutsceneStrings = new List<string>();

		if(forLevel == 0) {
			cutsceneStrings.Add("Hendrieta : \n\n\"Hello, Patrick.\"");
			cutsceneStrings.Add("Patrick : \n\n\"Whoa, Mrs. Hendrieta. What are you doing here?\"");
			cutsceneStrings.Add("Hendrieta : \n\n\"Let me remind you, Patrick, that if you don’t pay off your loan this year, your store will be closed.\"");
			cutsceneStrings.Add("Patrick : \n\n\"...yes I understand, Mrs...\"");
			cutsceneStrings.Add("Hendrieta : \n\n\"Okay then, I’ll leave.\"");
		} else if(forLevel == 1) {
			cutsceneStrings.Add("Patrick : \n\n\"Welcome to Patrick's Parcell! \nHow can I help you?\"");
			cutsceneStrings.Add("Sarah : \n\n\"Hello, Mr. Patrick! Can I get a parcell, please?\"");
			cutsceneStrings.Add("Patrick : \n\n\"Sure, Ms. Sarah! What kind of parcell do you need?\"");
			cutsceneStrings.Add("Sarah : \n\n\"A new year parcell, for a very special friend.\nI want to make it really fun.\"");
			cutsceneStrings.Add("Patrick : \n\n\"Don’t worry Ms. Sarah, I will not let you down.\"");
		} else if(forLevel == 2) {
			cutsceneStrings.Add("Patrick : \n\n\"Welcome to Patrick’s Parcell! How can i help you?\"");
			cutsceneStrings.Add("Bob : \n\n\"Hello, Mr. Patrick! Can I get a parcell, please?\"");
			cutsceneStrings.Add("Patrick : \n\n\"Sure, Mr. Bob! What kind of parcell do you need?\"");
			cutsceneStrings.Add("Bob : \n\n\"This is a parcell for a very special someone. I want to make it really romantic and sweet.\"");
			cutsceneStrings.Add("Patrick : \n\n\"Don’t worry Mr. Bob, your girlfriend will really love this parcell.\"");
		} else if(forLevel == 3) {
			cutsceneStrings.Add("Patrick : \n\n\"Welcome to Patrick’s Parcell! How can i help you?\"");
			cutsceneStrings.Add("Lisa : \n\n\"Hello, Mr. Patrick! Can I get a parcell, please?\"");
			cutsceneStrings.Add("Patrick : \n\n\"Sure, Ms. Lisa! What kind of parcell do you need?\"");
			cutsceneStrings.Add("Lisa : \n\n\"I want to make a parcell for my talented friend, Boby the musician.\"");
			cutsceneStrings.Add("Patrick : \n\n\"Don’t worry Ms. Lisa, I will not let you down.\"");
		} else if(forLevel == 4) {
			cutsceneStrings.Add("Patrick : \n\n\"Welcome to Patrick’s Parcell! How can i help you?\"");
			cutsceneStrings.Add("Lisa : \n\n\"Hello, Mr. Patrick! Can I get another parcell, please?\"");
			cutsceneStrings.Add("Patrick : \n\n\"Sure, Ms. Lisa! What kind of parcell do you need?\"");
			cutsceneStrings.Add("Lisa : \n\n\"I want to make a parcell full of cute eggs and bunny!\"");
			cutsceneStrings.Add("Patrick : \n\n\"Don’t worry Ms. Lisa, please wait here.\"");
		}

		nextText.GetComponent<UnityEngine.UI.Text>().text = "" + cutsceneStrings[currentIndex];
	}

	void Update() {
		if(Input.GetMouseButtonUp(0)) {
			Notify("next.dialogue");
		}
	}

	override public void OnNotification(string p_event, Object p_target, params object[] p_data) {
		switch(p_event) {
			case "next.dialogue":
				if(currentIndex == cutsceneStrings.Count - 1) {
					Notify("start.level" + forLevel);
				} else {
					currentIndex++;
					nextText.GetComponent<UnityEngine.UI.Text>().text = "" + cutsceneStrings[currentIndex];
				}
				break;
            
		}
   
	}
}
