﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneController : ControllerX {
	
	override public void OnNotification(string p_event, Object p_target, params object[] p_data) {
		int levelToLoad;
		switch(p_event) {
			case "scene.start":
				Log("Scene [" + p_data[0] + "][" + p_data[1] + "] started");
				break;
			case "scene.load":
				Log("Scene [" + p_data[0] + "][" + p_data[1] + "] loaded");
				break;
			case "start.level0":
				app.SceneLoad("Level0", new string[] { "level0" });
				break;
			case "start.level1":
				app.SceneLoad("Level1", new string[] { "level1" });
				break;
			case "start.level2":
				app.SceneLoad("Level2", new string[] { "level2" });
				break;
			case "start.level3":
				app.SceneLoad("Level3", new string[] { "level3" });
				break;
			case "start.level4":
				app.SceneLoad("Level4", new string[] { "level4" });
				break;
			case "start.level5":
				app.SceneLoad("Level5", new string[] { "level5" });
				break;
			case "start.adrian":
				app.SceneLoad("Adrian", new string[] { "level1" });
				break;
			case "start.chris":
				app.SceneLoad("Chris", new string[] { "level1" });
				break;
			case "start.lily":
				app.SceneLoad("Lily", new string[] { "level1" });
				break;
			case "start.mgs":
				app.SceneLoad("Mgs", new string[] { "level1" });
				break;
			case "start.shylla":
				app.SceneLoad("Shylla", new string[] { "level1" });
				break;
			case "application.exit":
				Log("exit");
				Application.Quit();
				break;
			case "next.level":
				levelToLoad = int.Parse(SceneManager.GetActiveScene().name.Substring(5, 1)) + 1;
				Notify("start.level" + levelToLoad);
				break;
			case "play.again":
				levelToLoad = int.Parse(SceneManager.GetActiveScene().name.Substring(5, 1));
				Notify("start.level" + levelToLoad);
				break;
			case "start.levelmenu":
				app.SceneLoad("LevelMenu", new string[] { "levelmenu" });
				break;
			case "start.mainmenu":
				Notify("play.menu.bgm");
				app.SceneLoad("MainMenu", new string[] { "mainmenu" });
				break;
			case "start.settings":
				app.SceneLoad("Settings", new string[] { "settings" });
				break;
			case "open.levelmenu1":
				app.SceneLoad("LevelMenu-1");
				break;
			case "open.levelmenu2":
				app.SceneLoad("LevelMenu-2");
				break;
			case "open.levelmenu3":
				app.SceneLoad("LevelMenu-3");
				break;
			case "open.levelmenu4":
				app.SceneLoad("LevelMenu-4");
				break;
			case "open.levelmenu5":
				app.SceneLoad("LevelMenu-5");
				break;
			case "open.cutscene1":
				app.SceneLoad("Cutscene1");
				break;
			case "open.cutscene2":
				app.SceneLoad("Cutscene2");
				break;
			case "open.cutscene3":
				app.SceneLoad("Cutscene3");
				break;
			case "open.cutscene4":
				app.SceneLoad("Cutscene4");
				break;
		}
	}
}