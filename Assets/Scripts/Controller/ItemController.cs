﻿using UnityEngine;
using System.Collections;

public class ItemController : ControllerX {

	GameObject selectedItem;
	Vector3 selectedItemLastPos;

	public override void OnNotification(string p_event, Object p_target, params object[] p_data) {
		GameObject clickedItem;
		GameObject collided;
		switch(p_event) {
			case "will.select.item":
				clickedItem = ((ItemView)p_target).gameObject;
				if(selectedItem != null) {
					selectedItem.GetComponent<Outline>().enabled = false;
				}
				selectedItem = clickedItem;
				Notify("item.selected");
				selectedItem.GetComponent<Outline>().enabled = true;
				break;
			case "select.item":
				clickedItem = ((ItemView)p_target).gameObject;
				if(selectedItem == clickedItem && selectedItemLastPos.Equals(clickedItem.transform.position)) {
					Notify("rotate.item");
				} else {
					selectedItemLastPos = selectedItem.transform.position;
				}
				break;
			case "item.inserted":
				selectedItem.GetComponent<Outline>().enabled = false;
				selectedItem = null;
				Notify("item.unselected");
				break;
			case "rotate.item":
				selectedItem.transform.Rotate(0.0f, 0.0f, -45.0f);
				break;
			case "trigger.enter":
				collided = ((Collider2D)p_data[0]).gameObject;
				if(collided.CompareTag("Container")) {
					((TriggerView)p_target).gameObject.transform.parent.GetComponent<ItemView>().currentCollide++;
				}
				break;
			case "trigger.exit":
				collided = ((Collider2D)p_data[0]).gameObject;
				if(collided.CompareTag("Container")) {
					((TriggerView)p_target).gameObject.transform.parent.GetComponent<ItemView>().currentCollide--;
					((TriggerView)p_target).gameObject.transform.parent.GetComponent<ItemView>().canDrag = true;
					((TriggerView)p_target).gameObject.transform.parent.GetComponent<ItemView>().mouseDrag = true;    
				}
				break;
			case "hightlight.item.canInsert":
				selectedItem.GetComponent<Outline>().color = 1;
				break;
			case "hightlight.item.cannotInsert":
				selectedItem.GetComponent<Outline>().color = 0;
				break;
		}
	}
}
