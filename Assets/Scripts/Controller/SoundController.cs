﻿using UnityEngine;
using System.Collections;

public class SoundController : ControllerX {

	public GameObject sfxClick;

	override public void OnNotification(string p_event, Object p_target, params object[] p_data) {
		switch(p_event) {
			case "sfx.click":
				GameObject instance = Instantiate(sfxClick) as GameObject;
				instance.GetComponent<AudioSource>().Play();
				Destroy(instance, 1);
				break;
		}
	}
}