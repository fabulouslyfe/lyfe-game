﻿using UnityEngine;
using System.Collections;

public class BGMController : ControllerX {

	static BGMController instance;
	public AudioClip bgmMenu;
	public AudioClip bgmGame;
	private AudioSource audioSource;

	void Awake() {
		if(instance == null) {
			instance = this;
		} else if(instance != this) {
			Destroy(gameObject);
		}
		DontDestroyOnLoad(gameObject);
	}

	void Start() {
		audioSource = GetComponent<AudioSource>();
		Notify("play.menu.bgm");
	}

	void PlayMenuBGM() {
		if(audioSource.isPlaying) {
			audioSource.Stop();
		}
		audioSource.clip = bgmMenu;
		audioSource.Play();
	}

	void PlayGameBGM() {
		if(audioSource.isPlaying) {
			audioSource.Stop();
		}
		audioSource.clip = bgmGame;
		audioSource.Play();
	}

	public override void OnNotification(string p_event, Object p_target, params object[] p_data) {
		switch(p_event) {
			case "play.menu.bgm":
				PlayMenuBGM();
				break;
			case "play.game.bgm":
				PlayGameBGM();
				break;
		}
	}
}
